from django.shortcuts import render
from newsapi import NewsApiClient


def index(request):
    newsapi = NewsApiClient(api_key='03ea8fd2ffba454b8d0a185dbf1290b7')
    top = newsapi.get_top_headlines(language='en', country='in')
    l = top['articles']
    desc = []
    news = []
    img = []
    source = []
    publishedAt = []
    ref_url = []

    for i in range(len(l)):
        f = l[i]
        news.append(f['title'])
        source.append(f['source']['name'])
        desc.append(f['description'])
        img.append(f['urlToImage'])
        ref_url.append(f['url'])
        publishedAt.append(f['publishedAt'])
    mylist = zip(news, desc, img, source, publishedAt, ref_url)

    return render(request, 'index.html', context={"mylist": mylist})

# Create your views here.
